%include "lib.inc"
%include "colon.inc"
%include "words.inc"

%define buffer_size 0xFF

extern find_word

section .rodata
	error_400: db "Error: Overflow detected!", 0
	error_404: db "Error: Key is not found!", 0
	
section .bss
	input_buffer: resb buffer_size

section .text

global _start
_start:
	mov rdi, input_buffer
	mov rsi, buffer_size
	call read_word
	
	cmp rax, 0
	jz .err_400

	mov rdi, rax
	mov rsi, next_element
	push rdx
	call find_word
	pop rdx

	cmp rax, 0
	je .err_404
	
	mov rdi, rax
	add rdi, 8
	add rdi, 1
	add rdi, rdx
	call print_string
	call print_newline
	xor rdi, rdi
	call exit
	
	.err_400:
		mov rdi, error_400
		call print_error
		jmp .end

	.err_404:
		mov rdi, error_404
		call print_error
	
	.end:
		call print_newline
		mov rdi, 1
		call exit
