section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

exit: 
    mov rax, 60
    syscall

string_length:
    xor rax, rax

    .loop:
        cmp byte[rax+rdi], 0
        je .break
        inc rax
        jmp .loop

    .break:
        ret

print_string:
    mov rsi, 1
    jmp print

print_error:
    mov rsi, 2

print:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi

    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1

    syscall
    ret

print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    pop rdi

    mov rax, 1
    mov rdi, 1

    syscall
    ret

print_newline:
    xor rax, rax
    mov rdi, 10
    jmp print_char

print_uint:
    xor rax, rax
    mov rax, rdi
    mov r9, rsp
    mov r10, 10
    push 0

    .loop:
        mov rdx, 0
        div r10
        add rdx, 0x30
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jnz .loop
        jmp .execute

    .execute:
        mov rdi, rsp
        push r9
        call print_string
        pop r9
        mov rsp, r9
        ret

print_int:
    xor rax, rax
    cmp rdi, 0
    jge .cout

    push rdi
    mov rdi, 0x2D
    call print_char
    pop rdi
    neg rdi

    .cout:
        jmp print_uint

string_equals:
    xor rax, rax
    xor r8, r8

    .loop:
        mov r10b, byte [rdi + r8]
        cmp byte [rsi + r8], r10b
        jne .zero
        cmp r10b, 0
        je .first
        inc r8
        jmp .loop

    .first:
        mov r9, 1
        jmp .break

    .zero:
        mov r9, 0
        jmp .break

    .break:
        mov rax, r9
        ret

read_char:
    xor rax, rax
    push rax
    mov rdx, 1
    xor rdi, rdi
    mov rsi, rsp
    syscall
    pop rax
    ret

read_word:
    xor rcx, rcx

    .loop:
        push rcx
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        pop rcx

        cmp rax, 0
        jz .break

        cmp rax, 0x20
        jz .space

        cmp rax, 0x9
        jz .space

        cmp rax, 0xA
        jz .space

        cmp rcx, rsi
        jge .control

        mov [rdi+rcx], rax
        inc rcx

        jmp .loop

    .control:
            xor rax, rax
            xor rdx, rdx
            ret

    .space:
            cmp rcx, 0
            jz .loop
            jmp .break

    .break:
            mov byte[rdi+rcx], 0
            mov rax, rdi
            mov rdx, rcx
            ret

parse_uint:
    xor rax, rax
    xor r10, r10
    xor rsi, rsi

    .loop:
        mov rcx, 0xA
        mov sil, [rdi + r10]
        cmp sil, 0x39
        jg .break

        sub sil, 0x30
        jl .break

        inc r10
        mul rcx
        add rax, rsi
        jmp .loop

    .break:
        mov rdx, r10
        ret

parse_int:
    xor rax, rax
    xor rdx, rdx

    cmp byte[rdi], '-'
    je .negative_number
    jmp parse_uint

    .negative_number:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
        cmp rcx, rdx
        jz .control
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp r9b, 0
        inc rcx
        jz .break
        jmp .loop

    .control:
        xor rax, rax
        ret

    .break:
        mov rax, rcx
        ret
