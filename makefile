%.o: %.asm
	nasm -f elf64 $< -o $@

main: lib.o main.o dict.o
	ld -o $@ $^

.PHONY: clean
clean:
	rm *.o
	rm main
	clear