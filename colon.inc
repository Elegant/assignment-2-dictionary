%define next_element 0

%macro colon 2
	%ifid %2
		%2: dq next_element
		%define next_element %2
	%else
		%error "Error: Argument is not ID!"
	%endif

	%ifstr %1
		db %1, 0
	%else
		%error "Error: Argument is not string!"
	%endif
%endmacro