%include "lib.inc"

section .text

global find_word

find_word:
	.loop:
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi

		cmp rax, 1
		je .success

    		mov rsi, [rsi]
    		cmp rsi, 0
    		jnz .loop
  
  	.fail:
    		xor rax, rax
    		ret

  	.success:
    		mov rax, rsi
    		ret
